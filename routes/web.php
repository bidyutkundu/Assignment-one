<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create', function () {
    return view('grade.create');
});

Route::post('/result',function(Request $request){
    $grade=$message="";
    $name=$request->name;
    $mark=$request->mark;

    if( strlen($name)>0 && strlen($mark)>0) {


        if ($mark >= 80 && $mark <= 100) {
            $grade = "A+";

        }
        if ($mark >= 70 && $mark <= 79) {
            $grade = "A";

        }
        if ($mark >= 60 && $mark <= 69) {
            $grade = "A-";

        }
        if ($mark >= 50 && $mark <= 59) {
            $grade = "B";

        }
        if ($mark >= 40 && $mark <= 49) {
            $grade = "C";

        }
        if ($mark >= 33 && $mark <= 39) {
            $grade = "D";

        }
        if ($mark >= 0 && $mark <= 32) {
            $grade = "F";

        }
        if ($mark < 0 || $mark > 100) {
            $message= "please input valid number";

        }

    }else {

            echo $message="Name field and mark field both are require";

    }


return view('grade.grade',[ 'name'=>$name, 'grade'=>$grade, 'message'=>$message,'grade'=>$grade ]);

});